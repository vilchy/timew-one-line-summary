-- SPDX-FileCopyrightText: 2023 Artur Wilniewczyc <artur.wilniewczyc@gmail.com>
--
-- SPDX-License-Identifier: GPL-3.0-or-later

module Main (main) where

import TimewOneLineSummary (currentSummary)

main :: IO ()
main = getContents >>= currentSummary >>= either putStr putStr
