<!--
SPDX-FileCopyrightText: 2023 Artur Wilniewczyc <artur.wilniewczyc@gmail.com>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Timew-one-line-summary

Timew-one-line-summary is a command-line extension for [Timewarrior](https://timewarrior.net/) that displays a short time-tracking report in a single line. The one-line format allows you to use it in a status bar (for example [xmobar](https://codeberg.org/xmobar/xmobar), [MATE panel](https://github.com/mate-desktop/mate-panel)).

It displays the following fields:

* the last activity [tags](https://timewarrior.net/docs/tags/),
* the start and end time of the last activity,
* the duration of the last activity,
* the total duration,
* and if you set an hourly rate in [the configuration](#configuration-options), the total cost.

## Project dependencies

Before using timew-one-line-summary ensure you have [Timewarrior installed](https://timewarrior.net/docs/install/) and [its configuration directory created](https://timewarrior.net/docs/tutorial/#setup).

## Installation

1. Download timew-one-line-summary sources.

    To get the sources you can clone [the git repository](https://codeberg.org/vilchy/timew-one-line-summary)
    (you need to have git [installed](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)):

    ```shell
    git clone https://codeberg.org/vilchy/timew-one-line-summary.git
    cd timew-one-line-summary
    ```

    or you can download [the release](https://codeberg.org/vilchy/timew-one-line-summary/releases/tag/0.1.0) tarball:

    ```shell
    wget https://codeberg.org/vilchy/timew-one-line-summary/archive/0.1.0.tar.gz -O timew-one-line-summary.tar.gz
    tar xzf timew-one-line-summary.tar.gz
    cd timew-one-line-summary
    ```

2. Build and install using Cabal.

    You need to have at least Cabal version 3.0 [installed](https://www.haskell.org/cabal/#install-upgrade). To do this, you can use [GHCup](https://www.haskell.org/ghcup/) or your system’s package manager, for example:

    ```shell
    sudo apt install cabal-install    # Debian, Ubuntu
    sudo dnf install cabal-install    # Fedora
    sudo pacman -S cabal-install      # Arch
    ```

    You may want to refresh the Cabal package index:

    ```shell
    cabal update
    ```

    Run `timew extensions` to locate the extensions directory, usually it's `$HOME/.config/timewarrior/extensions` or `$HOME/.timewarrior/extensions`.

    Build and install timew-one-line-summary:

    ```shell
    cabal install --installdir=$HOME/.config/timewarrior/extensions  # change the path if needed
    ```

## Configuration options

You can set an hourly rate in Timewarrior [configuration file](https://timewarrior.net/reference/timew-config.7/):

```ini
hourly_rate=<RATE>
```

or by using `rc.hourly_rate=<RATE>` on the command line.

## Running timew-one-line-summary

```shell
timew [report] one-line-summary [<RANGE>] [<TAG>…​]
```

See Timewarrior documentation for [range syntax](https://timewarrior.net/docs/interval/) and [hints](https://timewarrior.net/docs/hints/).

Examples:

* Display summary for today:

    ```shell
    $ timew one-line-summary :day
    tag1, tag2 | 14:30:00-… | 0:18:03 | 5:59:24
    ```

* Display summary for today with cost based on hourly rate of 200:

    ```shell
    $ timew one-line-summary :day rc.hourly_rate=200
    tag1, tag2 | 14:30:00-… | 0:21:35 | 6:02:56 | 1210
    ```

* Display summary for the current month:

    ```shell
    $ timew one-line-summary :month
    tag1, tag2 | 14:30:00-… | 0:22:16 | 41:15:24
    ```

## Running tests

You can run tests with Cabal:

```shell
cabal test --test-show-details=direct
```

## How to get help

Create [an issue](https://codeberg.org/vilchy/timew-one-line-summary/issues) on Codeberg.

## License

All original source code is licensed under the
[GNU General Public License v3.0](LICENSES/GPL-3.0-or-later.txt).
All documentation is licensed under the
[Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).
