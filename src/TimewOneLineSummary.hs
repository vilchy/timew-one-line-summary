-- SPDX-FileCopyrightText: 2023 Artur Wilniewczyc <artur.wilniewczyc@gmail.com>
--
-- SPDX-License-Identifier: GPL-3.0-or-later
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StrictData #-}

{- |
Module      :  TimewOneLineSummary
Copyright   :  (c) Artur Wilniewczyc, 2023
License     :  GPL-3.0-or-later

Maintainer  :  artur.wilniewczyc@gmail.com
Stability   :  unstable
Portability :  non-portable (GHC extensions)

Generating a single-line time-tracking report from a string provided by Timewarrior.

See [Timewarrior documentation](https://timewarrior.net/docs/api/#input-format)
for the input format.
-}
module TimewOneLineSummary (
    currentSummary,
    summary,
) where

import Control.Error.Safe (lastErr)
import Data.Aeson ((.!=), (.:), (.:?))
import qualified Data.Aeson as Aeson
import qualified Data.Aeson.Types as Aeson.Types
import qualified Data.Foldable as Foldable
import qualified Data.List as List
import Data.Maybe (fromMaybe, mapMaybe)
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Encoding as Text.Encoding
import Data.Time (NominalDiffTime, TimeZone, UTCTime, ZonedTime)
import qualified Data.Time as Time
import qualified GHC.Generics
import qualified Text.Printf
import qualified Text.Read

data Interval = Interval
    { start :: UTCTime
    , end :: Maybe UTCTime
    , tags :: [String]
    }
    deriving (GHC.Generics.Generic, Show, Eq)

data Configuration = Configuration
    { hourlyRate :: Maybe Int
    , reportStart :: Maybe UTCTime
    , reportEnd :: Maybe UTCTime
    }

data Result = Result
    { totalDuration :: NominalDiffTime
    , cost :: Maybe Float
    , lastInterval :: Interval
    , lastDuration :: NominalDiffTime
    }
    deriving (Show, Eq)

{- |
Generate the report using the current time from the system clock.
-}
currentSummary :: String -> IO (Either String String)
currentSummary input = do
    tz <- Time.getCurrentTimeZone
    now <- Time.getCurrentTime
    return $ summary tz now input

{- | Generate the report for a given time and time zone.

==== __Examples__

Basic usage:

>>> import Data.Time
>>> let tz = read "+0100"
>>> let now = UTCTime (fromGregorian 2023 5 4) (timeOfDayToTime (TimeOfDay 13 17 19))
>>> summary tz now "\n[{\"start\": \"20230504T070102Z\", \"tags\": [\"tag1\"]}]"
Right "tag1 | 08:01:02-\8230 | 6:16:17 | 6:16:17"

The total cost is calculated when an hourly rate is set in the headers:

>>> summary tz now "hourly_rate: 200\n\n[{\"start\": \"20230504T070102Z\"}]"
Right " | 08:01:02-\8230 | 6:16:17 | 6:16:17 | 1254"

When no interval is given, a Left String is returned:

>>> summary tz now "\n[]"
Left "No data."
-}
summary :: TimeZone -> UTCTime -> String -> Either String String
summary tz now input =
    format tz now <$> (process now =<< parse input)

--------------------------------------------------------------------------------
-- parse
--------------------------------------------------------------------------------

parse :: String -> Either String ([Interval], Configuration)
parse input = do
    let textLines = Text.lines . Text.pack $ input
        (headerLines, bodyLines) = break (== "") textLines
        configuration = parseConfiguration headerLines
        body = Text.unlines bodyLines
    intervals <- Aeson.eitherDecodeStrict . Text.Encoding.encodeUtf8 $ body
    return (intervals, configuration)

parseConfiguration :: [Text] -> Configuration
parseConfiguration headerLines =
    let headers = mapMaybe splitHeaderLine headerLines
        hourlyRate = lookupHeader "hourly_rate" headers >>= Text.Read.readMaybe
        reportStart = lookupHeader "temp.report.start" headers >>= parseTime
        reportEnd = lookupHeader "temp.report.end" headers >>= parseTime
     in Configuration{hourlyRate, reportStart, reportEnd}

splitHeaderLine :: Text -> Maybe (Text, Text)
splitHeaderLine line =
    case Text.breakOn ": " . Text.strip $ line of
        (_key, "") -> Nothing
        (key, val) -> Just (key, Text.drop 2 val)

lookupHeader :: Text -> [(Text, Text)] -> Maybe String
lookupHeader key headers = do
    (_, valText) <- Foldable.find ((== key) . fst) headers
    return (Text.unpack valText)

instance Aeson.FromJSON Interval where
    parseJSON :: Aeson.Value -> Aeson.Types.Parser Interval
    parseJSON = Aeson.withObject "Interval" $ \obj -> do
        start <- obj .: "start" >>= parseTime
        end <- obj .:? "end" >>= mapM parseTime
        tags <- obj .:? "tags" .!= []
        return Interval{start, end, tags}

parseTime :: (MonadFail m) => String -> m UTCTime
parseTime = Time.parseTimeM True Time.defaultTimeLocale "%Y%m%dT%H%M%SZ"

--------------------------------------------------------------------------------
-- process
--------------------------------------------------------------------------------

process :: UTCTime -> ([Interval], Configuration) -> Either String Result
process now (intervals, Configuration{hourlyRate, reportStart, reportEnd}) = do
    let pastReportEnd = Foldable.find (<= now) reportEnd
        limitedIntervals = map (limitInterval reportStart pastReportEnd) intervals
        totalDuration = sum $ map (intervalDuration now) limitedIntervals
        cost = durationToCost totalDuration <$> hourlyRate
    lastInterval <- lastErr "No data." limitedIntervals
    let lastDuration = intervalDuration now lastInterval
    return Result{totalDuration, cost, lastInterval, lastDuration}

limitInterval :: Maybe UTCTime -> Maybe UTCTime -> Interval -> Interval
limitInterval reportStart reportEnd Interval{start, end, tags} =
    let limitedStart = maybe start (max start) reportStart
        limitedEnd = maybeMin end reportEnd
     in Interval{start = limitedStart, end = limitedEnd, tags}

maybeMin :: (Ord a) => Maybe a -> Maybe a -> Maybe a
maybeMin (Just a) (Just b) = Just (min a b)
maybeMin a Nothing = a
maybeMin Nothing b = b

intervalDuration :: UTCTime -> Interval -> NominalDiffTime
intervalDuration now Interval{start, end} =
    Time.diffUTCTime (fromMaybe now end) start

durationToCost :: NominalDiffTime -> Int -> Float
durationToCost totalDuration hourlyRate =
    fromIntegral (round totalDuration * hourlyRate) / 3600

--------------------------------------------------------------------------------
-- format
--------------------------------------------------------------------------------

format :: TimeZone -> UTCTime -> Result -> String
format tz now Result{totalDuration, cost, lastInterval, lastDuration} =
    let Interval{start, end, tags} = lastInterval
        lastIntervalStart = formatTime tz now start
        lastIntervalEnd = maybe "…" (formatTime tz start) end
        segments =
            [ List.intercalate ", " tags
            , concat [lastIntervalStart, "-", lastIntervalEnd]
            , formatTimeDiff lastDuration
            , formatTimeDiff totalDuration
            ]
                ++ maybe [] (wrap . formatCost) cost
     in List.intercalate " | " segments

formatTime :: TimeZone -> UTCTime -> UTCTime -> String
formatTime tz referenceTime time =
    let zonedTime = Time.utcToZonedTime tz time
        zonedRefTime = Time.utcToZonedTime tz referenceTime
        formatStr = if sameDay zonedRefTime zonedTime then "%T" else "%F %T"
     in Time.formatTime Time.defaultTimeLocale formatStr zonedTime

sameDay :: ZonedTime -> ZonedTime -> Bool
sameDay time1 time2 =
    let localDay = Time.localDay . Time.zonedTimeToLocalTime
     in localDay time1 == localDay time2

formatTimeDiff :: NominalDiffTime -> String
formatTimeDiff diff =
    let (rest, seconds) = floor diff `divMod` (60 :: Integer)
        (hours, minutes) = rest `divMod` 60
     in Text.Printf.printf "%d:%02d:%02d" hours minutes seconds

wrap :: a -> [a]
wrap val = [val]

formatCost :: Float -> String
formatCost = show . (round :: Float -> Int)
