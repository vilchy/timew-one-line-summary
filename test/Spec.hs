-- SPDX-FileCopyrightText: 2023 Artur Wilniewczyc <artur.wilniewczyc@gmail.com>
--
-- SPDX-License-Identifier: GPL-3.0-or-later
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Main (main) where

import Data.Either (isLeft, isRight)
import Data.List (intercalate)
import qualified Data.Text as Text
import qualified Data.Time as Time
import Test.Hspec (
    Spec,
    context,
    describe,
    hspec,
    it,
    shouldBe,
    shouldEndWith,
    shouldSatisfy,
    shouldStartWith,
 )
import Text.RawString.QQ (r)
import qualified TimewOneLineSummary

summary :: String -> Either String String
summary =
    let tz = read "+0100"
        now =
            Time.UTCTime
                (Time.fromGregorian 2023 5 5)
                (Time.timeOfDayToTime (Time.TimeOfDay 13 17 19))
     in TimewOneLineSummary.summary tz now

main :: IO ()
main = hspec $ do
    describe "TimewOneLineSummary.summary" $ do
        context "when there is at least one interval" $ do
            summaryWithIntervalsSpec

        context "when there are no intervals" $ do
            it "returns 'No data' error" $ do
                let input = prepareInput [] "[]"
                summary input `shouldBe` Left "No data."

        context "when input is not valid" $ do
            it "returns an error" $
                summary "bad" `shouldSatisfy` isLeft

summaryWithIntervalsSpec :: Spec
summaryWithIntervalsSpec = do
    context "when the last interval has no tags" $ do
        let exampleIntervals =
                [r|[{
                    "id": 1,
                    "start": "20230505T070102Z"
                }]|]

        it "returns an empty first segment" $ do
            rightSegmentExample [] exampleIntervals 0 (`shouldBe` "")

    context "when the last interval has multiple tags" $ do
        let exampleIntervals =
                [r|[{
                    "id": 1,
                    "start": "20230505T070102Z",
                    "tags": ["multi t", "t2"]
                }]|]

        it "returns the tags separated by commas" $ do
            rightSegmentExample [] exampleIntervals 0 (`shouldBe` "multi t, t2")

    context "when the last interval tags have multi-byte characters" $ do
        let exampleIntervals =
                [r|[{
                    "id": 1,
                    "start": "20230505T070102Z",
                    "tags": ["żółć"]
                }]|]

        it "returns the tags with the multi-byte characters preserved" $ do
            rightSegmentExample [] exampleIntervals 0 (`shouldBe` "żółć")

    context "when the last interval starts today" $ do
        let exampleIntervals =
                [r|[{
                    "id": 1,
                    "start": "20230505T070102Z"
                }]|]

        it "returns the start time without the date" $ do
            rightSegmentExample [] exampleIntervals 1 (`shouldStartWith` "08:01:02-")

    context "when the last interval starts earlier than today" $ do
        let exampleIntervals =
                [r|[{
                    "id": 1,
                    "start": "20230504T070102Z"
                }]|]

        it "returns the start time along with the date" $ do
            rightSegmentExample [] exampleIntervals 1 (`shouldStartWith` "2023-05-04 08:01:02-")

    context "when the last interval starts today in local time but yesterday in UTC" $ do
        let exampleIntervals =
                [r|[{
                    "id": 1,
                    "start": "20230504T233002Z"
                }]|]

        it "returns the start time without the date" $ do
            rightSegmentExample [] exampleIntervals 1 (`shouldStartWith` "00:30:02-")

    context "when the last interval starts earlier than the report starts" $ do
        let exampleIntervals =
                [r|[{
                    "id": 1,
                    "start": "20230504T070102Z"
                }]|]
        let headers = ["temp.report.start: 20230504T230000Z"]

        it "returns the report start as the start time" $ do
            rightSegmentExample headers exampleIntervals 1 (`shouldStartWith` "00:00:00-")

    context "when the last interval is actively tracked" $ do
        let exampleInterval =
                [r|[{
                    "id": 1,
                    "start": "20230505T070102Z"
                }]|]

        context "when the report is not specified" $ do
            let headers = []

            it "returns an ellipsis in place of the end time" $ do
                rightSegmentExample headers exampleInterval 1 (`shouldEndWith` "-…")

        context "when the the report ends before the current time" $ do
            let headers = ["temp.report.end: 20230505T090305Z"]

            it "returns the report end as the end time" $ do
                rightSegmentExample headers exampleInterval 1 (`shouldEndWith` "-10:03:05")

        context "when the report ends after the current time" $ do
            let headers = ["temp.report.end: 20230505T230000Z"]

            it "returns an ellipsis in place of the end time" $ do
                rightSegmentExample headers exampleInterval 1 (`shouldEndWith` "-…")

    context "when the last interval ends on the same day as it starts" $ do
        let exampleIntervals =
                [r|[{
                    "id": 1,
                    "start": "20230504T070102Z",
                    "end": "20230504T090305Z"
                }]|]

        it "returns the end time without the date" $ do
            rightSegmentExample [] exampleIntervals 1 (`shouldEndWith` "-10:03:05")

    context "when the last interval ends on a different day than it starts" $ do
        let exampleIntervals =
                [r|[{
                    "id": 1,
                    "start": "20230504T070102Z",
                    "end": "20230505T090305Z"
                }]|]

        it "returns the end time along with the date" $ do
            rightSegmentExample [] exampleIntervals 1 (`shouldEndWith` "-2023-05-05 10:03:05")

    context "when the last interval ends on the same day as it starts in local time but not in UTC" $ do
        let exampleIntervals =
                [r|[{
                    "id": 1,
                    "start": "20230504T233002Z",
                    "end": "20230505T002001Z"
                }]|]

        it "returns the end time without the date" $ do
            rightSegmentExample [] exampleIntervals 1 (`shouldEndWith` "-01:20:01")

    context "when the last interval ends later than the report ends" $ do
        let exampleIntervals =
                [r|[{
                    "id": 1,
                    "start": "20230504T070102Z",
                    "end": "20230505T010203Z"
                }]|]
        let headers = ["temp.report.end: 20230504T230000Z"]

        it "returns the report end as the end time" $ do
            rightSegmentExample headers exampleIntervals 1 (`shouldEndWith` "-2023-05-05 00:00:00")

    let exampleIntervals =
            [r|[{
                    "id": 2,
                    "start": "20230504T070102Z",
                    "end": "20230504T090305Z"
                },
                {
                    "id": 1,
                    "start":  "20230505T131511Z"
                }]|]

    context "when the report range is not specified" $ do
        it "returns the last interval duration" $ do
            rightSegmentExample [] exampleIntervals 2 (`shouldBe` "0:02:08")

        it "returns the total duration of all intervals" $ do
            rightSegmentExample [] exampleIntervals 3 (`shouldBe` "2:04:11")

    context "when the report range is specified and ends before the current time" $ do
        let headers =
                [ "temp.report.start: 20230504T080000Z"
                , "temp.report.end: 20230505T131702Z"
                ]

        it "returns the last interval duration limited by the report range" $ do
            rightSegmentExample headers exampleIntervals 2 (`shouldBe` "0:01:51")

        it "returns the total duration limited by the report range" $ do
            rightSegmentExample headers exampleIntervals 3 (`shouldBe` "1:04:56")

    context "when the report range is specified and ends after the current time" $ do
        let headers =
                [ "temp.report.start: 20230504T080000Z"
                , "temp.report.end: 20230505T191702Z"
                ]

        it "returns the last interval duration" $ do
            rightSegmentExample headers exampleIntervals 2 (`shouldBe` "0:02:08")

        it "returns the total duration of all intervals" $ do
            rightSegmentExample headers exampleIntervals 3 (`shouldBe` "1:05:13")

    context "when hourly rate is not provided" $ do
        it "returns 4 segments" $ do
            let input = prepareInput [] exampleIntervals
            rightValue <- shouldBeRight $ summary input
            let segments = splitIntoSegments rightValue
            length segments `shouldBe` 4

    context "when hourly rate is provided" $ do
        let headers = ["hourly_rate: 200"]

        it "returns 5 segments" $ do
            let input = prepareInput headers exampleIntervals
            rightValue <- shouldBeRight $ summary input
            let segments = splitIntoSegments rightValue
            length segments `shouldBe` 5

        it "returns rounded total cost" $ do
            rightSegmentExample headers exampleIntervals 4 (`shouldBe` "414")

rightSegmentExample :: [String] -> [Char] -> Int -> (String -> IO b) -> IO b
rightSegmentExample headers intervals segmentIndex expectation = do
    let input = prepareInput headers intervals
    rightValue <- shouldBeRight $ summary input
    let selectedSegment = splitIntoSegments rightValue !! segmentIndex
    expectation selectedSegment

prepareInput :: [String] -> String -> String
prepareInput headers jsonBlock =
    let headersBlock = intercalate "\n" ("debug: off" : headers)
     in headersBlock ++ "\n\n" ++ jsonBlock

shouldBeRight :: (Show a) => Either a a -> IO a
shouldBeRight actual = do
    actual `shouldSatisfy` isRight
    return (either id id actual)

splitIntoSegments :: String -> [String]
splitIntoSegments text =
    let segments = Text.splitOn " | " . Text.pack $ text
     in map Text.unpack segments
